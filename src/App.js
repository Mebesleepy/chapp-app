import React from "react";
import "./App.css";
import NavigationBar from "./components/Navigationbar";
import { Route, Switch } from "react-router-dom";
import Register from "./views/Register";
import Chatroom from "./views/Chatroom";

function App() {
  return (
    <div className="App">
      <NavigationBar />
      <Switch>
        <Route exact path="/" component={Register} />
        <Route path="/chatroom" component={Chatroom} />
      </Switch>
    </div>
  );
}

export default App;
