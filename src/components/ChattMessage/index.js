import React from "react";
import { ListGroup } from "react-bootstrap";
import FontAwesomeIcon from "react-fontawesome";

const ChatMessage = ({
  time,
  info,
  user,
  message,
  privateMessage,
  userColor,
  userImage,
}) => {

  if (privateMessage) {
    return (
      <ListGroup.Item style={{ color: userColor }}>
        <FontAwesomeIcon name={userImage} /> {time} private message from: {user}{" "}
        {message}
      </ListGroup.Item>
    );
  }

  if (info) {
    return (
      <ListGroup.Item style={{ color: userColor }} variant="success">
        <FontAwesomeIcon name={userImage} /> {time} {user} {message}
      </ListGroup.Item>
    );
  }

  return (
    <ListGroup.Item style={{ color: userColor }}>
      <div>
        <FontAwesomeIcon name={userImage} /> {time} {user}
      </div>
      {message}
    </ListGroup.Item>
  );
};

export default ChatMessage;
