import React from "react";
import { Link } from "react-router-dom";
import { Nav } from "react-bootstrap";

const NavigationBar = () => {
  return (
    <Nav variant="tabs" defaultActiveKey="/">
      <Nav.Item>
        <Nav.Link>
          <Link to="/">Registration</Link>
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link>
          <Link to="/chatroom">Chatroom</Link>
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
};

export default NavigationBar;
