import axios from "axios";

export function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export const generateToken = async (userData) => {
  try {
    let resp = await axios.get("http://localhost:5010/sign", {
      headers: { "username": userData.username, "image": userData.image, "color": userData.color}
    } );

    const token = resp.data;

    return token;
  } catch (e) {
    console.log("something went wrong mate: " + e);
  }
};

export const verifyToken = async (token) => {
  try {
    let resp = await axios.get("http://localhost:5010/verify", {
      headers: {
        token: token,
      },
    });
    return resp;
  } catch (e) {
    return null;
  }
};
