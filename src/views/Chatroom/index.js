import React, { Component } from "react";
import ChattMessage from "../../components/ChattMessage";
import socketIoClient from "socket.io-client";
import { Button, Container, Row, Col, ListGroup, Form } from "react-bootstrap";
import { withRouter } from "react-router-dom";

const ENDPOINT = "http://127.0.0.1:5010";

class Chatroom extends Component {
  state = {
    messages: [],
    loggedIn: [],
    message: "",
    username: "",
    userImage: "",
    textColor: "",
  };

  componentDidMount() {
    this.socket = socketIoClient(ENDPOINT, {
      transportOptions: {
        polling: {
          extraHeaders: {
            jwt: window.sessionStorage.getItem("token"),
          },
        },
      },
    });

    this.socket.emit("new user", {
      time: new Date().toLocaleTimeString(),
      //username
      user: "",
      msg: "has joned the channel",
      info: true,
    });

    this.socket.on("chatt message", (msg) => {
      this.setState({ messages: [...this.state.messages, msg] });
    });

    this.socket.on("private message", (msg) => {
      this.setState({ messages: [...this.state.messages, msg] });
    });

    this.socket.on("update logged in", (users) => {
      this.setState({ loggedIn: [...users] });
    });
  }

  componentWillUnmount() {
    this.socket.disconnect();
  }

  postMessage = () => {
    if (this.state.message) {
      const messageToSend = {
        time: new Date().toLocaleTimeString(),
        user: this.state.username,
        color: this.state.textColor,
        image: this.state.userImage,
        msg: this.state.message,
      };

      this.socket.emit("chatt message", messageToSend);
      // we do not get our own messae from server insted we add it directly here to "save the trouble for the server"
      this.setState({
        messages: [...this.state.messages, messageToSend],
        message: "",
      });
    }
  };

  sendPrivateMessage(user) {
    const text = prompt(`what to say to ${user.username}`);

    const msg = {
      privateMessage: true,
      time: new Date().toLocaleTimeString(),
      user: this.state.username,
      color: this.state.textColor,
      image: this.state.userImage,
      to: user.id,
      msg: text,
    };

    this.socket.emit("private message", msg);
  }

  render() {
    const chatMessages = this.state.messages.map((message, i) => (
      <ChattMessage
        privateMessage={message.privateMessage}
        key={i}
        info={message.info}
        time={message.time}
        user={message.user}
        userColor={message.color}
        userImage={message.image}
        message={message.msg}
      />
    ));

    const loggedIn = this.state.loggedIn.map((user, i) => (
      <ListGroup.Item onClick={() => this.sendPrivateMessage(user)} key={i}>
        {user.username}
      </ListGroup.Item>
    ));

    return (
      <Container fluid>
        <Row>
          <Col md={4}>
            <ListGroup variant="flush">
              <ListGroup.Item>Users in chat</ListGroup.Item>
              {loggedIn}
            </ListGroup>
          </Col>
          <Col md={8}>
            <Row>
              <Col>
                <ListGroup>{chatMessages}</ListGroup>
              </Col>
            </Row>
            <Row noGutters>
              <Col>
                <Form.Control
                  className="mg-1"
                  onChange={(e) => this.setState({ message: e.target.value })}
                  value={this.state.message}
                  type="text"
                  placeholder="Enter message..."
                />
              </Col>
              <Col>
                <Button variant="success" onClick={() => this.postMessage()}>
                  Send
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withRouter(Chatroom);
