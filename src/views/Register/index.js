import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Form, Button, Container, Row } from "react-bootstrap";
import { generateToken } from "../../utils";
import FontAwesomeIcon from "react-fontawesome";

class Register extends Component {
  state = {
    name: "",
    image: "",
    color: "",
  };

  enterChatt() {
    if (this.state.name) {
      generateToken(this.state).then((responseToken) => {
        window.sessionStorage.setItem("token", responseToken);
      });

      this.props.history.push("/chatroom");
    }
  }

  setImage(image) {
    this.setState({ image: image });
  }

  setColor(color) {
    this.setState({ color: color });
  }

  render() {
    return (
      <Container fluid>
        <Row className="justify-content-center">
          <Form>
            <Form.Group controlId="username">
              <Form.Label>Username</Form.Label>
              <Form.Control
                onChange={(e) => this.setState({ name: e.target.value })}
                value={this.state.name}
                type="text"
                placeholder="Enter username"
              />
            </Form.Group>
            <Form.Label>Pick a "image"</Form.Label>
            <Form.Group controlId="imagePicker">
              <FontAwesomeIcon
                onClick={() => this.setImage("rocket")}
                name="rocket"
              />
              <FontAwesomeIcon
                onClick={() => this.setImage("anchor")}
                name="anchor"
              />
              <FontAwesomeIcon
                onClick={() => this.setImage("bug")}
                name="bug"
              />
              <FontAwesomeIcon
                onClick={() => this.setImage("flask")}
                name="flask"
              />
            </Form.Group>

            <Form.Label>Pick a text color</Form.Label>
            <Form.Group controlId="imagePicker">
              <div onClick={() => this.setColor("blue")}>Blue</div>
              <div onClick={() => this.setColor("red")}>Red</div>
              <div onClick={() => this.setColor("Yellow")}>Yellow</div>
            </Form.Group>

            <Button
              onClick={() => this.enterChatt()}
              variant="primary"
              type="button"
            >
              Enter chat
            </Button>
          </Form>
        </Row>
      </Container>
    );
  }
}

export default withRouter(Register);
